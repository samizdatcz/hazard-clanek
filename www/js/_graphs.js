Highcharts.setOptions({
    lang: {
        months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
        weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
        shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
        thousandsSep: ' ',
        decimalPoint: ',',
        rangeSelectorZoom: 'Zobrazit'
    }
});
$(function () {
    $('.highcharts.vsazeno').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Kolik Češi vsadí?',
        },
        subtitle: {
            text: 'Dominují hrací automaty (VLT a VHP), rychle rostou online kurzové sázky<br>Přesná čísla mohou být vyšší, nelegálně se podle protidrogového koordinátora prosází další desítky miliard',
        },
        xAxis: {
            type: 'datetime',
             tickInterval: 31557600000
        },
        yAxis: {
            title: {
                text: 'miliardy Kč'
            },
            min: 0
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x: %Y}: {point.y} miliard Kč'
        },
        exporting: {
                    enabled: false
        },
        colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
        plotOptions: {
            series: {
                    compare: 'value'
                }
        },
        credits: {
            enabled: true,
            href : "",
            text : "Zdroj: Národní monitorovací středisko pro drogy a závislosti"
        },
        series: [{
            name: 'VLT',
            data: [[Date.UTC(2002,1,1),2.07],[Date.UTC(2003,1,1),5.42],[Date.UTC(2004,1,1),9.01],[Date.UTC(2005,1,1),11.99],[Date.UTC(2006,1,1),14.85],[Date.UTC(2007,1,1),22.16],[Date.UTC(2008,1,1),35.91],[Date.UTC(2009,1,1),47.58],[Date.UTC(2010,1,1),56.67],[Date.UTC(2011,1,1),66.86],[Date.UTC(2012,1,1),65.20],[Date.UTC(2013,1,1),63.78],[Date.UTC(2014,1,1),66.30],[Date.UTC(2015,1,1),57.55]],
            marker: {
                enabled: false
            }
        }, {
            name: 'VHP',
            data: [[Date.UTC(2002,1,1),41.20],[Date.UTC(2003,1,1),44.16],[Date.UTC(2004,1,1),47.76],[Date.UTC(2005,1,1),50.10],[Date.UTC(2006,1,1),52.76],[Date.UTC(2007,1,1),57.17],[Date.UTC(2008,1,1),63.35],[Date.UTC(2009,1,1),48.73],[Date.UTC(2010,1,1),37.81],[Date.UTC(2011,1,1),30.86],[Date.UTC(2012,1,1),24.17],[Date.UTC(2013,1,1),14.62],[Date.UTC(2014,1,1),15.61],[Date.UTC(2015,1,1),32.17]],
            marker: {
                enabled: false
            }
        }, {
            name: 'kurzové sázky online',
            data: [[Date.UTC(2009,1,1),5.84],[Date.UTC(2010,1,1),7.66],[Date.UTC(2011,1,1),9.88],[Date.UTC(2012,1,1),14.41],[Date.UTC(2013,1,1),22.46],[Date.UTC(2014,1,1),29.40],[Date.UTC(2015,1,1),38.05]],
            marker: {
                enabled: false
            }
        }, {
            name: 'kurzové sázky na papíře',
            data: [[Date.UTC(2002,1,1),9.54],[Date.UTC(2003,1,1),11.19],[Date.UTC(2004,1,1),10.63],[Date.UTC(2005,1,1),11.38],[Date.UTC(2006,1,1),12.29],[Date.UTC(2007,1,1),11.97],[Date.UTC(2008,1,1),11.94],[Date.UTC(2009,1,1),8.82],[Date.UTC(2010,1,1),7.71],[Date.UTC(2011,1,1),6.55],[Date.UTC(2012,1,1),7.65],[Date.UTC(2013,1,1),6.60],[Date.UTC(2014,1,1),6.25],[Date.UTC(2015,1,1),5.83]],
            marker: {
                enabled: false
            }
        }, {
            name: 'loterie',
            data: [[Date.UTC(2002,1,1),6.81],[Date.UTC(2003,1,1),6.96],[Date.UTC(2004,1,1),6.02],[Date.UTC(2005,1,1),6.92],[Date.UTC(2006,1,1),7.56],[Date.UTC(2007,1,1),7.26],[Date.UTC(2008,1,1),7.46],[Date.UTC(2009,1,1),7.28],[Date.UTC(2010,1,1),6.84],[Date.UTC(2011,1,1),3.95],[Date.UTC(2012,1,1),6.65],[Date.UTC(2013,1,1),8.29],[Date.UTC(2014,1,1),11.60],[Date.UTC(2015,1,1),11.16]],
            marker: {
                enabled: false
            }
        }, {
            name: 'hry v kasinu',
            data: [[Date.UTC(2002,1,1),10.07],[Date.UTC(2003,1,1),10.05],[Date.UTC(2004,1,1),10.98],[Date.UTC(2005,1,1),10.07],[Date.UTC(2006,1,1),10.47],[Date.UTC(2007,1,1),9.63],[Date.UTC(2008,1,1),9.77],[Date.UTC(2009,1,1),9.56],[Date.UTC(2010,1,1),8.92],[Date.UTC(2011,1,1),8.69],[Date.UTC(2012,1,1),6.88],[Date.UTC(2013,1,1),8.12],[Date.UTC(2014,1,1),8.87],[Date.UTC(2015,1,1),7.40]],
            marker: {
                enabled: false
            }
        }]
    });
});

$(function () {
    $('.highcharts.zavislost').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Hazardní závislost'
        },
        subtitle: {
            text: 'Závislí v psychiatrických léčebnách nejčastěji bojují s automaty, přibývá ale obětí kurzových sázek',
        },
        xAxis: {
            categories: ['2013', '2015']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Závislí na hazardu'
            }
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '<b>{point.y} %</b> závislých trápí tento typ hazardu'
        },
        exporting: {
                    enabled: false
        },
        colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
        plotOptions: {
            column: {
                stacking: 'percent'
            }
        },
        credits: {
            enabled: false,
            href : "",
            text : "Zdroj: NMS"
        },
        series: [{
            name: 'hrací automaty',
            data: [82.5, 67.2]
        }, {
            name: '„pomalé“ kurzové sázky',
            data: [9.1, 12.0]
        }, {
            name: 'živé kurzové sázky',
            data: [0.9, 7.7]
        }, {
            name: 'karty',
            data: [1.3, 4.2]
        }, {
            name: 'hry v kasinu',
            data: [2.2, 1.9]
        }, {
            name: 'ostatní',
            data: [3.9, 7.0]
        }]
    });
});

$(function () {

    $('.highcharts.uspesnost').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Sázky a výhry'
        },
        subtitle: {
            text: 'Poměr mezi vsazenými penězi a vyplacenými výhrami'
        },
        xAxis: {
            categories: ["karetní turnaje v kasinu","kurzové sázky online","elektromechanická ruleta","VHP","živé hry v kasinu","kurzové sázky na papíře","VLT","lokální VLT","stírací losy","číselné loterie"],
            title: {
                text: null
            }
        },
        yAxis: {
            title: {
                text: '',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            },
        },
        tooltip: {
            valueSuffix: ''
        },
        exporting: {
            enabled: false
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>Poměr sázek a výher: ' + this.point.y + '</b><br>Vsazeno: ' + this.point.sazky + ' miliard Kč<br> Vyplaceno: ' + this.point.vyhry + ' miliard Kč';
            }
        },
        credits: {
            enabled: false,
            href : "",
            text : "Zdroj: NMS"
        },
        legend: {
            enabled: false
        },
        colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
        series: [{
            name: 'Poměr sázek a výher',
            data: [
                 {y: 0.93, sazky: 1.23, vyhry: 1.15},
                 {y: 0.89, sazky: 38.05, vyhry: 33.88},
                 {y: 0.88, sazky: 6.12, vyhry: 5.38},
                 {y: 0.88, sazky: 32.18, vyhry: 28.16},
                 {y: 0.85, sazky: 6.17, vyhry: 5.23},
                 {y: 0.75, sazky: 5.79, vyhry: 4.32},
                 {y: 0.74, sazky: 49.02, vyhry: 36.15},
                 {y: 0.73, sazky: 2.29, vyhry: 1.66},
                 {y: 0.57, sazky: 1.96, vyhry: 1.11},
                 {y: 0.50, sazky: 8.75, vyhry: 4.38}
            ]
        }]
    });
});

$(function () {

    $('.highcharts.povoleni').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Licence na hazard',
        },
        subtitle: {
            text: 'VHP, povolované přímo obcemi, rychle mizí. VLT, které spravuje ministerstvo financí, ubývá pomaleji.',
        },
        xAxis: {
            type: 'datetime',
             tickInterval: 31557600000
        },
        yAxis: {
            title: {
                text: 'licence na provoz her'
            },
            min: 0
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x: %Y}: {point.y} '
        },
        exporting: {
                    enabled: false
        },
        colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
        plotOptions: {
            series: {
                    compare: 'value'
                }
        },
        credits: {
            enabled: false,
            href : "",
            text : "Zdroj: NMS"
        },
        series: [{
            name: 'VLT',
            data: [[Date.UTC(2009,1,1),29594],[Date.UTC(2010,1,1),46572],[Date.UTC(2011,1,1),62926],[Date.UTC(2012,1,1),64188],[Date.UTC(2013,1,1),48461],[Date.UTC(2014,1,1),45467],[Date.UTC(2015,1,1),41670]],
            marker: {
                enabled: false
            }
        }, {
            name: 'VHP (licence MF)',
            data: [[Date.UTC(2009,1,1),3964],[Date.UTC(2010,1,1),4270],[Date.UTC(2011,1,1),3973],[Date.UTC(2012,1,1),3509],[Date.UTC(2013,1,1),3438],[Date.UTC(2014,1,1),4129],[Date.UTC(2015,1,1),4260]],
            marker: {
                enabled: false
            }
        }, {
            name: 'VHP (licence obcí)',
            data: [[Date.UTC(2009,1,1),47912],[Date.UTC(2010,1,1),34038],[Date.UTC(2011,1,1),24738],[Date.UTC(2012,1,1),17908],[Date.UTC(2013,1,1),15000],[Date.UTC(2014,1,1),12131],[Date.UTC(2015,1,1),10108]],
            marker: {
                enabled: false
            }
        }, {
            name: 'živé hry v kasinu',
            data: [[Date.UTC(2009,1,1),1083],[Date.UTC(2010,1,1),1314],[Date.UTC(2011,1,1),2064],[Date.UTC(2012,1,1),3571],[Date.UTC(2013,1,1),2760],[Date.UTC(2014,1,1),2741],[Date.UTC(2015,1,1),2254]],
            marker: {
                enabled: false
            }
        }]
    });
});

$(function () {

    $('.highcharts.prijmy').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Příjmy hazardních firem',
        },
        subtitle: {
            text: 'Zisky z automatů v posledních letech mírně klesají, nahrazují je online kurzové sázky',
        },
        xAxis: {
            type: 'datetime',
             tickInterval: 31557600000
        },
        yAxis: {
            title: {
                text: '%'
            },
            min: 0,
            reversedStacks: false
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '<b>{point.percentage:.1f} % příjmů</b> ({point.y} miliard Kč)'
        },
        exporting: {
                    enabled: false
        },
        colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
        plotOptions: {
            bar: {
                stacking: 'percent'
            }
        },
        credits: {
            enabled: false,
            href : "",
            text : "Zdroj: NMS"
        },
        series: [{
            name: 'VLT',
            data: [[Date.UTC(2002,1,1),0.58],[Date.UTC(2003,1,1),1.12],[Date.UTC(2004,1,1),1.71],[Date.UTC(2005,1,1),2.15],[Date.UTC(2006,1,1),2.88],[Date.UTC(2007,1,1),4.32],[Date.UTC(2008,1,1),8.19],[Date.UTC(2009,1,1),11.58],[Date.UTC(2010,1,1),14.60],[Date.UTC(2011,1,1),17.39],[Date.UTC(2012,1,1),16.42],[Date.UTC(2013,1,1),16.55],[Date.UTC(2014,1,1),17.83],[Date.UTC(2015,1,1),14.30]],
            marker: {
                enabled: false
            }
        }, {
            name: 'VHP',
            data: [[Date.UTC(2002,1,1),10.30],[Date.UTC(2003,1,1),11.04],[Date.UTC(2004,1,1),11.94],[Date.UTC(2005,1,1),12.52],[Date.UTC(2006,1,1),11.61],[Date.UTC(2007,1,1),12.53],[Date.UTC(2008,1,1),13.60],[Date.UTC(2009,1,1),12.15],[Date.UTC(2010,1,1),8.75],[Date.UTC(2011,1,1),6.71],[Date.UTC(2012,1,1),4.09],[Date.UTC(2013,1,1),2.60],[Date.UTC(2014,1,1),2.46],[Date.UTC(2015,1,1),4.02]],
            marker: {
                enabled: false
            }
        }, {
            name: 'kurzové sázky online',
            data: [[Date.UTC(2009,1,1),0.73],[Date.UTC(2010,1,1),0.99],[Date.UTC(2011,1,1),1.32],[Date.UTC(2012,1,1),1.94],[Date.UTC(2013,1,1),2.49],[Date.UTC(2014,1,1),3.33],[Date.UTC(2015,1,1),4.17]],
            marker: {
                enabled: false
            }
        }, {
            name: 'kurzové sázky na papíře',
            data: [[Date.UTC(2002,1,1),1.20],[Date.UTC(2003,1,1),1.09],[Date.UTC(2004,1,1),1.48],[Date.UTC(2005,1,1),1.34],[Date.UTC(2006,1,1),1.48],[Date.UTC(2007,1,1),1.87],[Date.UTC(2008,1,1),2.46],[Date.UTC(2009,1,1),2.60],[Date.UTC(2010,1,1),2.27],[Date.UTC(2011,1,1),2.05],[Date.UTC(2012,1,1),2.03],[Date.UTC(2013,1,1),1.58],[Date.UTC(2014,1,1),1.77],[Date.UTC(2015,1,1),1.49]],
            marker: {
                enabled: false
            }
        }, {
            name: 'loterie',
            data: [[Date.UTC(2002,1,1),3.31],[Date.UTC(2003,1,1),3.48],[Date.UTC(2004,1,1),3.08],[Date.UTC(2005,1,1),3.56],[Date.UTC(2006,1,1),3.93],[Date.UTC(2007,1,1),3.68],[Date.UTC(2008,1,1),3.85],[Date.UTC(2009,1,1),3.67],[Date.UTC(2010,1,1),3.67],[Date.UTC(2011,1,1),1.98],[Date.UTC(2012,1,1),3.51],[Date.UTC(2013,1,1),3.86],[Date.UTC(2014,1,1),4.59],[Date.UTC(2015,1,1),5.41]],
            marker: {
                enabled: false
            }
        }, {
            name: 'hry v kasinu',
            data: [[Date.UTC(2002,1,1),2.26],[Date.UTC(2003,1,1),2.11],[Date.UTC(2004,1,1),2.16],[Date.UTC(2005,1,1),1.97],[Date.UTC(2006,1,1),1.93],[Date.UTC(2007,1,1),1.91],[Date.UTC(2008,1,1),1.82],[Date.UTC(2009,1,1),1.72],[Date.UTC(2010,1,1),1.54],[Date.UTC(2011,1,1),1.65],[Date.UTC(2012,1,1),1.31],[Date.UTC(2013,1,1),1.49],[Date.UTC(2014,1,1),1.40],[Date.UTC(2015,1,1),1.03]],
            marker: {
                enabled: false
            }
        }]
    });
});

$(function () {
    $('.highcharts.prevalence').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Zkušenost s hazardem'
        },
        subtitle: {
            text: 'Nejrychleji roste zkušenost s online hazardem ve skupině 15-24 let',
        },
        xAxis: {
            categories: ['online hazard, 15+', 'online hazard, 15-24', 'online hazard, ženy', 'online hazard, muži', 'kurzové sázky, 15+', 'kurzové sázky, 15-24', 'kurzové sázky, ženy', 'kurzové sázky, muži', 'automaty/kasino, 15+', 'automaty/kasino, 15-24', 'automaty/kasino, ženy', 'automaty/kasino, muži']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jaká část skupiny si v posledním roce zkusila hazard'
            }
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '<b>{point.y} %</b> skupiny si v uplynulém roce vyzkoušelo tento typu hazardu'
        },
        exporting: {
                    enabled: false
        },
        colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
        credits: {
            enabled: false,
            href : "",
            text : "Zdroj: NMS"
        },
        series: [{
            name: '2013',
            data: [2.5,2.5,0.2,4.9,10.7,14.7,2.6,19.3,4.7,13.4,1.6,8.0]
        }, {
            name: '2015',
            data: [8.2,18.3,1.9,15.0,16.3,22.8,5.6,27.6,5.9,17.2,2.0,10.0]
        }]
    });
});
