---
title: "Hazardní byznys roste. Automaty mizí, nahrazují je nebezpečné online sázky"
perex: "Čeští hazardní hráči loni vsadili rekordních 152 miliard korun. Trh s rizikem roste navzdory úspěšné regulaci hracích automatů. Mladší generace hráčů teď má problém, který neumí nikdo řešit: hernu v podobě mobilu si nosí v kapse, online hazard je stále návykovější a regulovat ho je obtížné."
authors: ["Jan Boček", "Jan Cibulka"]
published: "21. července 2016"
coverimg: https://samizdat.cz/data/hazard-clanek/media/automaty.jpeg
url: "hazard"
libraries: [jquery, highcharts]
---

<aside class="small">
  <figure>
    <div class="highcharts vsazeno" style="height:600px"></div>
  </figure>
</aside>

Před sedmi lety vsadili čeští sázkaři na sportovní utkání přes internet 5,8 miliardy korun, loni to bylo 38 miliard. Ukazují to data z hazardní ročenky [Národního monitorovacího střediska](http://www.drogy-info.cz/data/obj_files/32148/715/VZhazard2015_web.pdf), kterou ve středu vydala vláda.

Nárůst online hazardu přináší problémy, které souvisejí právě s přesunem do nového prostředí. Dobře je ilustrují například změny v podobě kurzových sázek. Ty byly dlouho považovány za poměrně bezpečný druh hazardu.

Jenže přesun z „kamenných“ provozoven na web charakter kurzového sázení změnil. Postupně zmutovalo do rizikové podoby takzvaných *živých sázek*. Na většině sázkařských webů dnes lze přímo během zápasu sázet na každý detail hry: u tenisu na vítěze každého míčku, u hokeje nebo fotbalu na to, kdo vsítí následující branku, a podobně. Oproti klasickým kurzovým sázkám není třeba čekat na výsledek, u živých sázek je známý téměř okamžitě. Během jednoho zápasu tak hráč může vsadit desítky „tiketů“.

„Kurzové sázky, včetně živých, nemají finanční limity,“ upozorňuje národní protidrogový koordinátor Jindřich Vobořil. „Jakmile začnete sázet tisícovky, je jedno, jestli na výsledek čekáte desítky sekund nebo minuty. Výplatu prohrajete za jeden zápas.“

Nedávno přijatý hazardní zákon, který začne platit v příštím roce, situaci nezmění. Limity pro online sázky nepřináší, stanoví pouze pravidla pro dobrovolnou regulaci hráčů samotných.

„Zákon o hazardních hrách obsahuje řadu sebeomezujících opatření, mezi jinými povinnost provozovatele kurzových sázek umožnit účastníkovi hazardní hry nastavit si maximální výši sázek za jeden den, kalendářní měsíc, maximální výši čisté denní prohry a čisté měsíční prohry,“ upozorňuje náměstek ministra financí Ondřej Závodský, který má na starost dohled nad loteriemi.

Podle Vobořila ale taková opatření nejsou dostatečná. „Kromě prohraných peněz je ještě potřeba vzít v úvahu riziko vzniku závislosti; to je u živých sázek poměrně vysoké,“ varuje. „Závislost na hazardu může vzniknout rychle. Každá sázka vyvolává vzrušení, hráč zažívá změnu vědomí. Neurologické výzkumy ukazují, že zážitek ze hry u patologického hráče se z devadesáti procent překrývá s heroinovým raušem. U hracích automatů se prožitek opakuje každé dvě až tři sekundy, proto jsou tak nebezpečné. Živé sázky můžou hráče vtáhnout do podobně rychlého kolotoče.“

<aside class="small">
  <figure>
    <div class="highcharts zavislost" style="height:500px"></div>
  </figure>
</aside>

Jeho slova potvrzuje i výzkum mezi pacienty, které závislost na hazardu přivedla do psychiatrické léčebny. Stále sice platí, že většinu z nich trápí především hrací automaty, jejich podíl ale klesá. Rychle naopak přibývá pacientů, kteří bojují se závislostí na kurzových sázkách. V roce 2013 jich byla desetina, loni už každý pátý.

Online sázky mají ještě jednu magnetickou vlastnost: lepší poměr vsazených a vyhraných peněz než kurzové sázky v kamenných provozovnách. Zatímco u papírových sázek se českým hráčům souhrnně vrátí pouze 75 procent vkladů, na webu je to 90 procent.

Podle Vobořila může za rozdílný poměr vsazených a vyplacených peněz hlavně fakt, že přes online sázky proteče větší množství peněz. Hazardním firmám se tak vyplatí nabídnout na webu sázky s výhodnějšími kurzy – provoz pro ně nepředstavuje téměř žádné náklady a celková prohraná částka je i přes lepší kurzy vysoká.

<div class="highcharts uspesnost" style="height:600px"></div>

## Česko, ráj hracích automatů

Pro pochopení novinek na trhu s hazardem je potřeba vrátit se o nějakých deset let zpátky, do zlatého věku hracích automatů. Tehdy se vedle klasických hospodských automatů – jazykem zákona <i>výherních hracích přístrojů</i> – začaly prosazovat <i>videoloterijní terminály</i>. Na první pohled stejné přístroje mají ovšem několik zásadních rozdílů.

|                  | VHP                    | VLT                                  |
|------------------|------------------------|--------------------------------------|
| Maximální sázka  | obvykle 5 Kč           | obvykle 1 000 Kč                     |
| Technické řešení | samostatný přístroj    | terminál napojený na vzdálený server |
| Povoluje         | obec                   | ministerstvo financí                 |
| Jackpot          | pro konkrétní přístroj | sdílený mezi přístroji i hernami     |

Videoloterijní terminály přinesly řadu novinek: například algoritmy, které se umí přizpůsobit stylu hry jednotlivých hráčů. Dohromady s vyšší maximální sázkou dokážou obrat hráče o výplatu rychleji než klasické automaty.

K tomu se přidaly blikající cedule před hernou, lákající na jackpot v hodnotě desítek tisíc korun. Ten ovšem můžou sdílet stovky automatů v mnoha hernách a šance na výhru jackpotu se tak podobá spíš klasické loterii.

Licenci k provozu videoloterijních terminálů navíc neudělovaly obce, ale ministerstvo financí. To si při povolování počínalo poměrně bezstarostně, takže řadu míst nový typ automatů rychle zaplavil. Obce, které nesly značnou část následků problémového hráčství, se zpočátku neměly rozhodnutí ministerstva jak bránit.

<aside class="small">
  <figure>
    <div class="highcharts povoleni" style="height:400px"></div>
  </figure>
</aside>

Vrchol zažívali provozovatelé hracích automatů v roce 2011. V hernách, hospodách nebo sportbarech jich poblikávalo přes 90 tisíc, většinu tvořily právě podle odborníků nebezpečnější videoloterijní terminály. Do automatů Češi v tomto roce naházeli téměř 100 miliard korun a zisky hazardních firem z herních přístrojů se vyšplhaly přes 24 miliard.

Jenže ve stejném roce dospěl k Ústavnímu soudu spor Františkových Lázní s ministerstvem financí. V kostce šlo o to, zda smějí obce na svém území regulovat vyhláškou všechny automaty včetně videoloterijních terminálů. Ústavní soud [dal za pravdu Františkovým Lázním](http://www.rozhlas.cz/zpravy/politika/_zprava/944883), téměř okamžitě ovšem v novém loterijním zákoně obce o pravomoc zakázat automaty opět přišly. O dva roky později, pro změnu po sporu ministerstva s klatovskou radnicí, se [ústavní soudci zastali obcí podruhé](http://www.usoud.cz/aktualne/ustavni-soud-dnes-vyhlasil-nalez-jimz-potvrdil-pravomoc-obci-regulovat-provozovani-hazardu/), tentokrát už úspěšně.

Následoval razantní odliv hazardních přístrojů. Pro ilustraci: výherních hracích přístrojů, o kterých rozhodují výhradně obce, zmizela od roku 2011 víc než polovina.

„Automaty začaly mizet z iniciativy obcí,“ shrnuje národní protidrogový koordinátor Jindřich Vobořil. „Šlo o výsledek bitvy aktivistů s ministerstvem, pomohlo jim rozhodnutí Ústavního soudu, automaty se opakovaně staly volebním tématem. Části měst v Brně nebo Praze rozhodly o přísných regulacích až prohibici.“

Videoloterijní terminály mizí pomaleji. Ministerstvo financí se sice po opakovaném rozhodnutí soudu pustilo do jejich rušení, ale nepředřelo se. Od rekordního roku 2011 jich ubyla třetina, řada podnětů obcí ke zrušení heren ovšem čeká na ministerské razítko dodnes.

„Naše městská část dala podnět ke zrušení všech videoloterijních terminálů na svém území někdy kolem roku 2013,“ vypráví Martin Dutkiewič, zastupitel na brněnských Vinohradech. „Od té doby čekáme, až ministerstvo zareaguje. Nelíbí se nám to, razíme nulovou toleranci hazardu a musíme občanům neustále vysvětlovat, proč u nás zůstávají tři herny. Přitom s nimi nemůžeme nic dělat,“ stěžuje si.

Podle náměstka ministra financí Ondřeje Závodského, který v roce 2014 nastoupil na ministerstvo s protihazardním programem, se ovšem rušení automatů zrychluje. Na svém blogu [vyčíslil počet zrušených automatů](http://blog.aktualne.cz/blogy/ondrej-zavodsky.php?itemid=27516) od svého nástupu na 17 tisíc.

Na konci roku 2015 zůstávalo v provozu 56 tisíc hracích automatů, na konci letošního června to bylo 51 tisíc. V některých městech herny téměř zmizely, stále se naopak koncentrují kolem německé a rakouské hranice.

<aside class="big">
  <iframe src="https://samizdat.cz/data/herny-2016-mapa/" style="height:600px"></iframe>
</aside>

<i>V mapě chybí asi pět procent heren. Jde o podniky, u nichž v evidenci ministerstva financí chybí přesná adresa.</i>

S klesajícími počty licencí spadly také příjmy hazardních společností z hracích automatů. Od nejsilnějšího roku 2011 klesly o čtvrtinu.

## Nové hřiště: Internet

S úbytkem zisků z herních přístrojů se hazardní firmy začaly orientovat na online prostředí. Před pěti lety tvořily příjmy z hracích automatů téměř 80 procent všech jejich příjmů; dnes je to jen mírně nad polovinu. Stále větší část zisků přináší právě online hazard.

<aside class="big">
  <div class="highcharts prijmy" style="height:600px"></div>
</aside>

Dosud platný hazardní zákon ovšem nechával pro hazard na webu jen malý prostor. Legální byly pouze online kurzové sázky, licenci navíc mohly dostat jedině české firmy. Ostatní hazardní hry a zahraniční společnosti byly na internetu tabu.

To ovšem neznamená, že by nepovolený hazard neexistoval. Naopak, šedá zóna nelicencovaných her je poměrně široká. „Odhadujeme, že se na webu nelegálně prosází téměř stejně jako legálně,“ upřesňuje Viktor Mravčík z Národního monitorovacího střediska pro drogy a závislosti. „To jsou přibližně čtyři miliardy korun příjmů ze hry, na loterijní dani takhle státu uniká přibližně miliarda. Polovina z nelegálního hazardu je – velmi hrubým odhadem – poker, polovina kurzové sázky,“ vysvětluje Mravčík.

Odpovědnost za nelegální sázení přitom podle dosavadního zákona nesli samotní hráči. V roce 2014 udělilo ministerstvo financí hráči na nelegálním webu [pokutu 850 tisíc korun](http://www.mfcr.cz/cs/aktualne/tiskove-zpravy/2014/za-sazeni-u-zahranicnich-provozovatelu-p-17800).

Nelicencované hazardní firmy se navíc bránily tím, že Česká republika ustanoveními hazardního zákona porušuje evropské právo na volný pohyb služeb. Evropská komise kvůli tomu Česku hrozila soudem.

I proto na ministerstvu financí vznikla [novela hazardního zákona](https://www.zakonyprolidi.cz/cs/2016-186), platná od začátku příštího roku. Zavádí licence pro zahraniční firmy a umožňuje všechny typy her, které jsou povoleny offline – včetně hracích automatů.

„Legalizace online hazardu byla nutná,“ uvažuje protidrogový koordinátor Jindřich Vobořil. „Pro ministerstvo je klíčové omezit daňové úniky, pro nás zase možnost nějak hazard hlídat. Zákazy se nic nevyřeší,“ dodává.

„Velké firmy, jako je třeba gibraltarská BWIN, podle mě stejně s českými partnery dlouhodobě spolupracují,“ domnívá se Vobořil. „Jinak si neumím představit, že by tu třeba sponzorovaly veřejné akce. Ideální by bylo rozkrýt jejich strukturu. To ale nebude hned. Na ministerstvu vnitra sice vzniká ‚protihazardní kobra‘, ta se ale bude zaměřovat spíš na černé herny,“ načrtává další plány úředníků.

Podle Vobořila zároveň s legalizací online hazardu dochází k přepisování mapy hazardních firem. Společnosti, které ovládaly trh s hracími automaty, nejsou ty stejné, které provozují hazard na internetu.

## Nejohroženější jsou teenageři

Přesun na web zároveň hazardním firmám dodává nový typ hráčů: mladé lidi – obvykle muže – z bohatších sociálních skupin, než jsou typičtí hráči automatů.

Právě podíl mladých lidí, kteří si v uplynulém roce vyzkoušeli online hazard, dramaticky roste. Od roku 2013 se zvedl sedminásobně na dnešních 18 procent. V obecné populaci se k účasti na webovém hazardu v uplynulém roce přiznává 8 procent lidí. Častěji hrají muži.

<aside class="big">
  <div class="highcharts prevalence" style="height:600px"></div>
</aside>

Na webu tedy sází hlavně nejzranitelnější věková skupina. Zároveň je u online hazardu podle Národního monitorovacího střediska pro drogy a závislosti vysoké riziko návyku. Situaci ještě zhoršuje fakt, že online hráči nemusejí – na rozdíl od patologických uživatelů automatů – hledat hernu. Pokušení mají obvykle přímo v kapse.

„Určité procento lidí má predispozici k návykovému chování, často se k hazardu připojí problémy s alkoholem, nelegálními drogami a diskomfort v oblasti duševního zdraví,“ vysvětluje Vobořil. „Samotný sklon k závislosti nezměníme, ale můžeme ho včas detekovat a zmírnit jeho dopady.“

„Problém je, že právě na prevenci a následnou léčbu patologických hráčů stát nedává peníze,“ doplňuje. „Mírně se to zlepšilo v posledních dvou letech, ale ty prostředky jsou zatím velmi nedostatečné. Když už stát peníze z hazardu získá, jdou obvykle na mládežnický sport s tím, že to je přece prevence hazardu. Ale jak sport dělá prevenci hazardnímu hraní, mi nikdo nikdy nevysvětlil,“ pozastavuje se nad jedním z argumentů Vobořil.

## Online hazard má ohlídat novela

Rizika, spojená s online hazardem, má ohlídat novela loterijního zákona. Ta na jedné straně zavádí nekompromisní, [hojně](http://www.lupa.cz/clanky/jan-simkanic-kdo-lze-ten-cenzuruje/) [diskutovanou](http://video.aktualne.cz/dvtv/ministerstvo-financi-si-usurpovalo-moc-jako-v-rusku-hrozi-ce/r~d315188c022e11e6a5f4002590604f2e/) a zřejmě také neproveditelnou mimosoudní blokaci webových stránek, na druhou stranu je podle kritiků děravá a hazardní lobby příliš neoslabí.

První novinkou, kterou novela přináší, je zmíněná pravomoc ministerstva blokovat webové stránky a účty nelicencovaných her. Poskytovatelé internetového připojení budou mít povinnost nepustit své klienty na webové stránky, které ministerstvo financí uzná za nezákonné. Zákon ovšem neřeší, jak postupovat, pokud bude blokace neúčinná.

Paragrafy navíc nezmiňují například mobilní aplikace, výslovně úkolují pouze poskytovatele internetového připojení. Právě mobilní aplikace přitom provozovatelé nelicencovaného hazardu nabízejí už dnes. Podle Ondřeje Závodského z ministerstva financí se ovšem řešení nabízí.

„Zákon o hazardních hrách v sobě obsahuje základní povinnost zamezení přístupu k hazardní hře pro české spotřebitele, a to pod hrozbou pokuty až do výše 50 milionů korun,“ vysvětluje. „V České republice jsou v současné době čtyři hlavní distribuční platformy, a to App Store, Google Play, Windows Phone Store a BlackBerry App World. Vzhledem k počtu subjektů je možnost udělení pokuty zcela dostatečným nástrojem, jak zamezit nabídce nelegálních hazardních her.“

Řešením sporné situace by tedy v tomto případě byla hrozba pokutou například webovému gigantu Google. V případě neuposlechnutí by ministerstvo financí zřejmě nařídilo blokaci webu Google Play, a tedy nedostupnost jakékoliv aplikace pro mobilní platformu Android, provozovanou právě Googlem.

Zákon zavádí také řadu dalších opatření: zvyšuje hazardní daň, stanoví dvousekundovou pauzu mezi hrami na automatu, po dvou hodinách hraní nařizuje hráči čtvrthodinovou přestávku, zakazuje hernám provoz mezi třetí a desátou hodinou ranní a úplně ruší automaty mimo prostor heren a kasín.

Další novinkou je registr problémových hráčů. Ten by měl zabránit v online i offline hazardu osobám, které pobírají dávky v hmotné nouzi nebo je na ně vyhlášen úpadek.

Nová jsou také seberegulační opatření. Každý hráč si bude moci nastavit výši prohry, po které mu herna – kamenná i webová – neumožní další hru. Může se dokonce sám upsat k tomu, že načas hrát nechce; provozovatelé hazardních her mu v tom budou muset zabránit.

Novela se vypořádává také s takzvanými kvízomaty – herními automaty, které obcházejí zákaz hazardních zařízení. Hráče přivítají „vědomostní“ otázkou („kde se nachází Čínská zeď?“) a tím se podle provozovatelů vymykají definici hazardní hry. Podle současného hazardního zákona se kvízomaty ruší poměrně obtížně.

## Patologické hraní novela zřejmě neomezí

Závodský věří, že nový zákon pomůže hazard omezit. „Každý z nástrojů pro potírání nelegálního byznysu s online hazardem funguje tak na 80 či 90 procent,“ vysvětloval letos v květnu [v rozhovoru pro web Česká pozice](http://ceskapozice.lidovky.cz/v-boji-s-nelegalnim-hazardem-na-internetu-pomuze-i-blokace-webu-p74-/tema.aspx?c=A160429_181258_pozice-tema_lube). „A my potřebujeme namíchat vražedný koktejl pro boj s organizovaným zločinem. Tím koktejlem je právě kombinace všech těch nástrojů.“

Během dvou let připomínkování ovšem hazardní zákon ztratil hodně ze své původní síly. Už loni jeho kompromisní verzi [na Radiožurnálu kritizoval](http://www.rozhlas.cz/zpravy/politika/_zprava/ministerstvo-financi-zmekcuje-zakon-proti-hazardu-ustupuje-lobby-tvrdi-kritici--1489934) ředitel české pobočky Transparency International David Ondráčka. „Podle nás ministerstvo financí a konkrétně ministr dramaticky ustupuje hazardní lobby, zejména té její tvrdé části, která provozuje automaty a herny,“ prohlásil před rokem.

Adresovaný ministr financí Andrej Babiš to ovšem vidí podobně. „Je vidět, že hazardní lobby je skutečně vlivná a silná, protože všechny strany hlasovaly pro a jedině Hnutí proti,“ [komentoval v České televizi](http://www.ceskatelevize.cz/ct24/ekonomika/1756621-za-zmenou-danovych-sazeb-v-novem-zakone-stoji-vlivna-hazardni-lobby-rika-babis) letos v dubnu pozměňovací návrh, který snížil zdanění hazardu.

Polovičatá opatření zákonu vyčítá také protidrogový koordinátor Jindřich Vobořil. Podle něj navíc zákon postrádá některé klíčové nástroje.

„Navrhoval jsem udělit licenci jen těm hazardním firmám, které prokáží, že dělají prevenci patologickému hraní,“ vysvětluje Vobořil. „Hazardní firma by na požádání musela nejprve ukázat herní plán neboli algoritmus, kterým se automat či online herna řídí. Pokud by s jeho pomocí odhalila rizikového hráče, musela by se mu věnovat, bez toho by nedostala licenci. Tohle jsou nástroje, které pro regulaci hazardu používají vyspělé země. Do českého zákona se nedostaly.“

„Minimálně bych chtěl, aby hazardní firmy detekovaly patologické hraní a měly povinnost nabídnout informaci, že existuje nějaká léčba,“ dodává.

Doplňuje ho i šéf Národního monitorovacího střediska pro drogy a závislosti Viktor Mravčík. „Do zákona jsme chtěli dostat větší spoluzodpovědnost provozovatelů za sledování možných negativních dopadů her. Snad bude možnost tato opatření prosazovat v rámci navazujících vyhlášek nebo plánovaných novel zákona,“ věří.

Vobořil tak optimistický není. „Nechci stahovat kalhoty, když brod je ještě daleko. Moje povinnost je zkusit politiky přesvědčit. Je ale reálná hrozba, že se mi to u hazardu nepovede.“
